var app = app || {};

app.main = {
    headerLine: function () {
        var $line = $('.header-line');
        if ($line.length === 0) return false;
        $line.addClass('visible');
    },
    elementInViewport: function (el) {
        var rect = el.getBoundingClientRect ();
        var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
        return (
            (rect.top >= 0)
            && ((rect.top) <= windowHeight)
        );
    },
    scrollPage: function () {
        var $elements = $('[data-animate]');
        $elements.each(function (i, item) {
            var $item = $(item),
                delay = ($item.attr('data-delay')) ? $item.attr('data-delay') : false;
            if (app.main.elementInViewport(item) && !$item.hasClass('animate')) {
                var type = ($item.attr('data-animate')) ? $item.attr('data-animate') : ' ';
                if (!delay) {
                    $item.addClass('animate ' + type);
                } else {
                    setTimeout(function () {
                        $item.addClass('animate ' + type);
                    }, delay);
                }

            }
        });

        // var elements = document.querySelectorAll('[data-animate]');
        // for (var i = 0; i < elements.length; i++) {
        //     var item = elements[i];
        //     console.log(item.className);
        //     if (app.main.elementInViewport(item) && item.className !== 'animate') {
        //         var type = item.getAttribute('data-animate');
        //         var delay = +item.getAttribute('data-delay');
        //         item.className +=' animate '+type;
        //     }
        // }


    },
    initEvent: function () {
        document.addEventListener("scroll", this.scrollPage);
    },
    parallax: function () {
        var $parallax = $('.jsParallax');
        if ($parallax.length === 0) return false;
        $parallax.each(function (i, item) {
            var $item = $(item),
                speed = ($item.attr('data-speed')) ? $item.attr('data-speed') : -0.2,
                startY = ($item.attr('data-start-y')) ? (Math.round($item.outerHeight()/100*$item.attr('data-start-y'))) : 0;


            $item.parallax("50%", speed, true);
        });
    },
    scrollMagicParallax: function () {
        var $parallax = $('.ScrollMagicParallax');
        if ($parallax.length === 0) return false;
        var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: 'onEnter', duration: '200%'}});

        $parallax.each(function (i, item) {
            var $item = $(item);
            new ScrollMagic.Scene({triggerElement: $item})
                .on('progress', function (e) {
                    console.log('progress', e);
                })
                .on('update', function (e) {
                    console.log('update', e);
                })
                .on('start', function (e) {
                    console.log('start', e);
                })
                .setTween($item.find('.ScrollMagicParallax-image'), {y: '35%', ease: Linear.easeNone})
                .addTo(controller);
        });
    },
    fixedHeader: function () {
        if ($(window).width() <= 950) return false;
        var self = this,
            $headerMenu = $('.header__menu'),
            menuHeight = $headerMenu.height(),
            fixedFlag = false;
        function menuCompensatorAdd() {
            var html = '<div style="height:'+ menuHeight +'px;"></div>';
            $(html).insertBefore($headerMenu);
        }
        function menuCompensatorRemove() {
            $headerMenu.prev().remove();
        }

        var controller = new ScrollMagic.Controller();
        new ScrollMagic.Scene({ duration: menuHeight, offset: $('.header').height() + $('.intro').height()})
            .addTo(controller)
            .on('start', function (e) {
                if (!fixedFlag) {
                    menuCompensatorAdd();
                    $headerMenu.addClass('fixed');
                    fixedFlag = true;
                }
            })
            .on('leave', function (e) {
                if (fixedFlag) {
                    if (e.scrollDirection === 'REVERSE') {
                        menuCompensatorRemove();
                        $headerMenu.removeClass('fixed');
                        fixedFlag = false;
                    }
                }
            })
            .setTween($headerMenu, {y: menuHeight});
    },
    videoModal: function () {
        var self = this;
        var $fancyboxVideo = $('.jsFancyboxVideo');
        if ($fancyboxVideo.length === 0) return false;
        $fancyboxVideo.fancybox({
            slideClass: 'modal-video',
            beforeShow: function (e) {
                var $player = $(e.current.src).find('.player'),
                    $playOverlay = $player.find('.player__overlay'),
                    $playPause = $player.find('.player__play-pause');
                // $playOverlay.hide();
                // $playPause.removeClass('paused');
                // $player.addClass('playing');
                // setTimeout(function () {
                //     $player.off('mouseenter').one('mouseenter', function () {
                //         $playOverlay.fadeIn(400);
                //     });
                // }, 1000)

            },
            afterShow: function (e) {
                var $player = $(e.current.src).find('.player'),
                    $playPause = $player.find('.playpause'),
                    $video = $player.find('.player__video');

                // $video[0].play();
                $playPause.click();
            },
            afterClose: function (e) {
                var $player = $('.player'),
                    $playOverlay = $player.find('.player__overlay'),
                    $video = $player.find('.player__video'),
                    $playPause = $player.find('.player__play-pause');
                var video = $('.player__video')[0];
                $video[0].currentTime = 1.5;
                $video[0].pause();
                // $playPause.addClass('paused');
                // $player.removeClass('playing');
            }
        });
    },
    videoPlayer: function () {
        var $player = $('.player');
        if ($player.length === 0) return false;

        var $video = $player.find('.player__video'),
            video = $video[0],
            $controls = $player.find('.controls'),
            $progressHolder = $controls.find('.progressholder'),
            $buffered = $controls.find('.buffered'),
            $progress = $controls.find('.progress'),
            $progressorb = $controls.find('.progressorb'),
            $playPause = $controls.find('.playpause'),
            $fullscreen = $controls.find('.fullscreen'),
            $volume = $controls.find('.volume'),
            $volumeBtn = $controls.find('.volume-btn'),
            $volumeChangerWrap = $controls.find('.volume-changer-wrap'),
            $volumeChanger = $controls.find('.volume-changer'),
            $volumeLevel = $controls.find('.volume-level'),
            progressFlag = true,
            holderOffsetLeft = $progressHolder.offset().left,
            full = false;

        $progress.addClass('hidden');


        var loadCheck = setInterval(function(){
            if(video.duration > 0){
                $video[0].currentTime = 1.5;
                clearInterval(loadCheck);
            }
        }, 100);

        function toggleVideo() {
            if(!video.paused) {
                $player.removeClass('playing');
                $playPause.removeClass('paused');
                video.pause();
                $controls.fadeIn(200);
            } else {
                $player.addClass('playing');
                $playPause.addClass('paused');
                video.play();
                if (full) {
                    $controls.fadeOut(700);
                }
            }

            if (progressFlag) {
                $progress.removeClass('hidden');
                progressFlag = false;
            }
        }
        function toggleVolume() {
            $video.prop('muted', !$video.prop('muted'));
            $volume.toggleClass('muted');
        }
        function updateVolume(e) {
            var pos = e.offsetX,
                prop = pos / $volumeChanger.width();
            $volumeLevel.css('width', prop*100+'%');
            video.volume = prop;
        }
        function updateProgress() {
            var bp = video.buffered.end(video.buffered.length-1) / video.duration;
            var bw = bp * 100;
            $buffered.css("width", bw + "%");
            var p = video.currentTime / video.duration;
            var w = p * 100;
            $progress.css("width", w + "%");
            if(video.ended) {
                $playPause.removeClass('paused');

                progressFlag = true;
                $video[0].currentTime = 1.5;
                $progress.addClass('hidden');
            }
        }
        function updateOrb(e) {
            var pos = e.pageX - holderOffsetLeft;
            var prop = pos / $progressHolder.width();
            var prog = prop * video.duration;
            $progressorb.css('margin-left', pos + 'px');
        }
        setInterval(updateProgress, 100);

        function launchIntoFullscreen(element) {
            if(element.requestFullscreen) {
                element.requestFullscreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        }
        function exitFullscreen() {
            if(document.exitFullscreen) {
                document.exitFullscreen();
            } else if(document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if(document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
            $controls.fadeIn(500);
        }
        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function () {
            if( !(window.innerHeight === screen.height)) {
                $controls.fadeIn(500);
            }
        });

        $progressHolder.mousemove(function(e) { updateOrb(e); });
        $video.on('click', function (e) {
            e.preventDefault();
            toggleVideo();
        });
        $playPause.on('click', function (e) {
            e.preventDefault();
            toggleVideo();
        });
        $progressHolder.on('click', function(e) {
            var pos = e.pageX - holderOffsetLeft;
            var prop = (pos + 1) / $progressHolder.width();
            var prog = prop * video.duration;
            video.currentTime = prog;
            updateProgress();

            if (progressFlag) {
                $progress.removeClass('hidden');
                progressFlag = false;
            }
        });
        $fullscreen.on('click', function(e) {
            e.preventDefault();
            if(!full) launchIntoFullscreen($player[0]);
            else exitFullscreen($player[0]);
            full = !full;
        });
        $volumeBtn.on('click', function (e) {
            e.preventDefault();
            toggleVolume();
        });
        $volumeChanger.on('click', function (e) {
            e.preventDefault();
            updateVolume(e);
        });




        // $overlay.on('click', function (e) {
        //     e.preventDefault();
        //     var $this = $(this),
        //         $playPause = $this.find('.player__play-pause'),
        //         video = $video[0];
        //     if (video.paused) {
        //         $video[0].play();
        //     } else {
        //         $video[0].pause();
        //     }
        //     $playPause.toggleClass('paused');
        //     $player.toggleClass('playing');
        // });
    }
};


app.init = function () {
    app.main.headerLine();
    app.main.initEvent();
    app.main.scrollPage();
    app.main.fixedHeader();


    app.main.videoModal();
    app.main.videoPlayer();


};


window.onload = function () {
    app.init ();
};



$(function () {

  $("#speaker-slider").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      rows: 2,
      arrows: false,
      speed: 1000,
      autoplay: false,
      autoplaySpeed: 4000,
      dots: true,
      /*responsive: [
          {
            breakpoint: 1281,
            settings: {
              arrows: false,
              slidesToShow: 3
            }
          },
          {
            breakpoint: 870,
            settings: {
              arrows: false,
              slidesToShow: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              arrows: false,
              slidesToShow: 1
            }
          }
        ]*/
        
    });


  var clock = $('.clock').FlipClock(3600 * 24 * 3, {
    clockFace: 'DailyCounter',
    countdown: true,
    showSeconds: false
  });
	
	
	$('.burger-mob').on('click', function () {
	  $('.header__menu').addClass('active');
	  $('body').addClass('overflowHidden');
	});

	$('.mobile-menu-close').on('click', function () {
	  $('.header__menu').removeClass('active');
	  $('body').removeClass('overflowHidden');
	});


  $(".tabs-js").on("click", "li:not(.active)", function() {
         $(this)
           .addClass("active")
           .siblings()
           .removeClass("active")
           .closest(".tabs-info-js")
           .find(".tabs-list-js")
           .removeClass("active")
           .eq($(this).index())
           .addClass("active");
       });




	function pageWidget(pages) {
	  var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
	  widgetWrap.prependTo("body");
	  for (var i = 0; i < pages.length; i++) {
	  $('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
	  }
	  var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:5px 10px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
	  widgetStilization.prependTo(".widget_wrap");
	}



	$(document).ready(function ($) {pageWidget(['index', 'contacts', 'gallery', 'speakers', 'past-conference', 'details']);});
});